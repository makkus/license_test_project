[![PyPI status](https://img.shields.io/pypi/status/license_test_project.svg)](https://pypi.python.org/pypi/license_test_project/)
[![PyPI version](https://img.shields.io/pypi/v/license_test_project.svg)](https://pypi.python.org/pypi/license_test_project/)
[![PyPI pyversions](https://img.shields.io/pypi/pyversions/license_test_project.svg)](https://pypi.python.org/pypi/license_test_project/)
[![Pipeline status](https://gitlab.com/frkl/license_test_project/badges/develop/pipeline.svg)](https://gitlab.com/frkl/license_test_project/pipelines)
[![Code style](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/ambv/black)

# license_test_project

*Dummy Python project for license discovery*


## Description

Documentation still to be done.

# Development

Assuming you use [pyenv](https://github.com/pyenv/pyenv) and [pyenv-virtualenv](https://github.com/pyenv/pyenv-virtualenv) for development, here's how to setup a 'license_test_project' development environment manually:

    $ pyenv install 3.7.3
    $ pyenv virtualenv 3.7.3 license_test_project
    $ git clone https://gitlab.com/makkus/license_test_project
    $ cd <license_test_project_dir>
    $ # activate virtualenv, depending on your setup either:
    $ pyenv local license_test_project
    $ # or:
    $ pyenv activate license_test_project
    $ # or directly like so (this last one should always work):
    $ source ~/.pyenv/versions/license_test_project/bin/activate
    $ pip install -e '.[develop,testing,docs]'
    $ pre-commit install

    $ dummy-command

    Hello!
    ...
    ...
    ...


## Copyright & license

Please check the [LICENSE](/LICENSE) file in this repository (it's a short license!).

[Parity Public License 6.0.0](https://licensezero.com/licenses/parity)

[Copyright (c) 2019 frkl OÜ](https://frkl.io)
