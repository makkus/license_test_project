# -*- coding: utf-8 -*-
import click


@click.command()
def dummy_command():

    print("Hello!")

    import pkg_resources

    [
        print(package)
        for package in list(
            pkg
            for pkg in pkg_resources.working_set
            if (
                "Classifier: Programming Language :: Python :: 3"
                in list(
                    pkg.get_metadata(
                        "METADATA" if pkg.has_metadata("METADATA") else "PKG-INFO"
                    ).splitlines()
                )
            )
        )
    ]

    for package in pkg_resources.working_set:
        try:
            if package.has_metadata("LICENSE"):
                print(package.get_metadata("LICENSE"))
        except (Exception):
            pass
